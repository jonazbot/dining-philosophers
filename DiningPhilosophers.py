#! usr/bin/env python3

"""
This program demonstrates the Dining Philosophers problem with parallelism where
each philosopher is running as a separate Process from the multiprocessing module.
"""

__author__ = 'Jonas Valen'

from random import random
from sys import exit
from time import sleep
from multiprocessing import Semaphore, Process, Queue

STOP = False
NUM_DINERS = 6     # Number of Philosophers at the table.
SPEED = 0.2         # Additional time for thread sleep. 0 = off.
RANDOMIZATION = 2   # Randomization multiplier for thread sleep.
                    # 1 = multiplier off, 0 = randomization off.


class Philosopher(Process):
    """
    Philosopher class.  Gets assigned a seat and corresponding chopsticks.
    The philosopher then attempts to pick up both corresponding chopsticks.
    If one of them is taken, it will wait to eat until both chopsticks are
    available.  The philosopher eats for a random amount of time before putting
    both the chopsticks down and proceeds to start thinking for a random amount
    of time.
    """

    def __init__(self, seat_number, exclusion_lock):
        Process.__init__(self)
        self.table_manners = exclusion_lock
        self.seat = seat_number
        self.left_chopstick = chopsticks[seat_number]
        self.right_chopstick = chopsticks[(seat_number + 1) % NUM_DINERS]

    def run(self):
        while not STOP:
            queue.put(f'{self.name} is hungry.')
            try:
                self.table_manners.acquire()
                self.left_chopstick.acquire()
                queue.put(f'{self.name} picked up {self.left_chopstick.name} ')
                self.right_chopstick.acquire()
                queue.put(f'{self.name} picked up {self.right_chopstick.name} ')
                self.table_manners.release()
            except(KeyboardInterrupt):
                break
            queue.put(f'{self.name} is eating')
            try:
                sleep(random() * RANDOMIZATION + SPEED)
            except(KeyboardInterrupt):
                break
            self.left_chopstick.release()
            self.right_chopstick.release()
            queue.put(f'{self.name} is done using {self.left_chopstick.name} '
                      f'and {self.right_chopstick.name}')
            queue.put(f'{self.name} is thinking.')
            try:
                sleep(random() * RANDOMIZATION + SPEED)
            except(KeyboardInterrupt):
                break


if __name__ == "__main__":
    """
    Initializes the chopsticks as semaphores and names them corresponding 
    to the number of philosophers, then initializes the philosophers and 
    names them by seat number and starts the thread.
    """

    print(f'----- {NUM_DINERS} Dining Philosophers -----\n')

    chopsticks = {}
    for n in range(NUM_DINERS):
        chopstick = Semaphore(1)
        chopstick.name = f'Chopstick {n}'
        chopsticks[n] = chopstick

    table_manners = Semaphore(1)
    queue = Queue()
    diners = []
    for n in range(NUM_DINERS):
        philosopher = Philosopher(seat_number=n, exclusion_lock=table_manners)
        philosopher.name = f'Philosopher {n}'
        philosopher.daemon = True
        philosopher.start()
        diners.append(philosopher)

    while not STOP:
        try:
            print('Starting...\n')
            while True:
                print(queue.get())
        except (KeyboardInterrupt, SystemExit, InterruptedError):
            STOP = True
            SPEED = 0
            RANDOMIZATION = 0
            print('\nTerminating...')
        finally:
            for n in range(NUM_DINERS):
                diners[n].terminate()
            for n in range(NUM_DINERS):
                diners[n].join()
    exit()
